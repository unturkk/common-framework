# README #

### What is this repository for? ###

This is a generic framework that handles various kinds of errors passed from any API to it. 

It takes an error message in String format, splits it into multiple errors -if necessary- and matches those errors with corresponding records in ERRORCODE_CROSS_REF table.
Finally, it modifies the error description to make it more clear to the client and returns it in the following format:

{ 
    "errors": [ 
        { 
            "error_code": "1400-07-002", 
            "description": "The value for [base_currency_amount] cannot be a negative number.", 
            "field_name": "base_currency_amount", 
            "api_specification_url": "URL?" 
        }, 
        { 
            "error_code": "1400-06-001", 
            "description": "The value for [base_currency_code] must be between [3] and [3] characters.", 
            "field_name": "base_currency_code", 
            "field_value": "EURO", 
            "api_specification_url": 
            "URL?" 
         } 
    ] 
}

### IMPORTANT: ### This is so called 'Java Version' of the framework. It IS a Mule project, however, figuring out the error codes from the error message passed to it and changing the description of the errors by replacing placeholders with real values and field names are done with Java classes through Java components.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Kemal Unturk