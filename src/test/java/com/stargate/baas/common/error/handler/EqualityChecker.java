package com.stargate.baas.common.error.handler;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class EqualityChecker implements Callable {

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		String expected=eventContext.getMessage().getProperty("expectedPayload", PropertyScope.INVOCATION);
		String actual=eventContext.getMessage().getProperty("messageActual", PropertyScope.INVOCATION);
		
		if(expected.equals(actual)){
			return 1;
		}
		return -1;
	}

}
