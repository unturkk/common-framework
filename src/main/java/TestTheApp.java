import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.stargate.common.dao.ServicesDAO;
import com.stargate.common.entity.Services;

/* This class is just to test the DAO
 * and neither the DAO nor this class are
 * being used in anywhere in the application
 * 
 * In Mule Flow, the errors are being fetched
 * using a DB connector.
 */
public class TestTheApp {

	public static void main(String[] args) {

		ApplicationContext context =
	    		new ClassPathXmlApplicationContext("spring/baas-common-dao-context.xml");		
		ServicesDAO svcDAO = (ServicesDAO) context.getBean("servicesDAO");
		
		((ClassPathXmlApplicationContext)context).close();
		
		Services srv = new Services();		
		srv = svcDAO.getService();
		
		System.out.println("\nDEF_MASK_CHAR : " +srv.getDefaultMaskChar() +"\n");
	}
}
