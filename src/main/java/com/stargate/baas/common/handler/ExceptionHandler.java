package com.stargate.baas.common.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mule.api.MuleEventContext;
import org.springframework.stereotype.Component;

import com.stargate.baas.common.constant.ErrorCodes;
import com.stargate.baas.common.constant.Exceptions;

@Component
public class ExceptionHandler {

	public void setExceptionErrorAndMessage(String errorMessage, MuleEventContext eventContext) {

		// Error code list to be passed to DB
		List<String> errorCodeList = eventContext.getMessage().getInvocationProperty("varErrorCodeList");

		// List of maps for error key/value pairs
		List<Map<String, String>> listOfErrors = eventContext.getMessage().getInvocationProperty("varErrorMapList");

		Map<String, String> m = new HashMap<String, String>();

		// TODO Don't this in JSONValidationHandler
		// Try not use the same data structures that are used to handle
		// multiple errors in the same request
		if (errorMessage.contains(Exceptions.METHOD_NOT_ALLOWED_EXCEPTION)) {
			m.put("ErrorCode", ErrorCodes.err_1405_01_001);
			errorCodeList.add(ErrorCodes.err_1405_01_001);
			listOfErrors.add(m);
		} else if (errorMessage.contains(Exceptions.RESOURCE_NOT_FOUND_EXCEPTION)) {
			m.put("ErrorCode", ErrorCodes.err_1404_01_004);
			errorCodeList.add(ErrorCodes.err_1404_01_004);
			listOfErrors.add(m);
		} else { // Return unknown backend error
			m.put("ErrorCode", ErrorCodes.err_1500_01_004);
			errorCodeList.add(ErrorCodes.err_1500_01_004);
			listOfErrors.add(m);
		}
	}

}
