package com.stargate.baas.common.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.springframework.beans.factory.annotation.Autowired;

public class ErrorHandler implements Callable {

	@Autowired
	ErrorMessageHandler errorMessageHandler;
	
	@Autowired
	ExceptionHandler exceptionHandler;
	
	@Autowired
	JSONValidationHandler jsonValidationHandler;
	
	@Autowired
	AuthorizationErrorHandler authErrorHandler;

	public Object onCall(MuleEventContext eventContext) throws Exception {
		
		int httpStatusCode = 0;
		
		eventContext.getMessage().setInvocationProperty("varHttpStatusCode", httpStatusCode);	
		
		// Error message from incoming query parameter
		String errorMessage = eventContext.getMessage().getInvocationProperty("varErrorMessage");
		
		// List of maps for error key/value pairs
		// TODO Move to JSON Handler
		List<Map<String, String>> listOfErrors = errorMessageHandler.getErrorMapList(errorMessage);
		
		// Error code list to be passed to DB
		List<String> errorCodeList = new ArrayList<String>();
		
		// Add to varErrorCodeList flow variable which is used to query the DB
		eventContext.getMessage().setInvocationProperty("varErrorCodeList", errorCodeList);	
		
		// Add errors into varErrorMapList flow variable to be used
		// when placeholders are replaced
		eventContext.getMessage().setInvocationProperty("varErrorMapList", listOfErrors);
		
		if(errorMessage.contains("Json content is not compliant with schema")){
			httpStatusCode = jsonValidationHandler.setJSONValidationErrorsAndMessages(eventContext);
		} else {
			exceptionHandler.setExceptionErrorAndMessage(errorMessage, eventContext);
		}
		
		return eventContext.getMessage().getPayload();
	}
}
