package com.stargate.baas.common.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class ErrorMessageHandler {
	
	/*
	  Method that takes the incoming error message as an argument and splits it
	  to a list of key value pairs. Example error message is:
	  
	  --- BEGIN MESSAGES ---
		error: instance type (integer) does not match any allowed primitive type (allowed: ["string"])
		    level: "error"
		    schema: {"loadingURI":"..."}
		    instance: {"pointer":"/base_currency_code"}
		    domain: "validation"
		    keyword: "type"
		    found: "integer"
		    expected: ["string"]
		error: instance type (null) does not match any allowed primitive type (allowed: ["integer","number"])
		    level: "error"
		    schema: {"loadingURI":"..."}
		    instance: {"pointer":"/contra_currency_amount"}
		    domain: "validation"
		    keyword: "type"
		    found: "null"
		    expected: ["integer","number"]
		error: string "UeeD" is too long (length: 4, maximum allowed: 3)
		    level: "error"
		    schema: {"loadingURI":"..."}
		    instance: {"pointer":"/contra_currency_code"}
		    domain: "validation"
		    keyword: "maxLength"
		    value: "UeeD"
		    found: 4
		    maxLength: 3
		---  END MESSAGES  ---
	 */

	public List<Map<String, String>> getErrorMapList(String errorMessage) {
		
		// List of maps for error key/value pairs
		List<Map<String, String>> listOfErrors = new ArrayList<Map<String, String>>();

		String[] errorMessageList = null;
		
		// Split the message into separate errors
		if(errorMessage != null && errorMessage.contains("error:")) {
			errorMessageList = errorMessage
					.substring(errorMessage.indexOf("error:") + 6, errorMessage.indexOf("---  END MESSAGES  ---"))
					.split("error:");
		}
		
		// Iterate through all errors
		// to contruct a map
		if(errorMessageList != null){
			for (int i = 0; i < errorMessageList.length; i++) {
	
				// TODO
				// Should not need to add 'error' back
				errorMessageList[i] = "error:" + errorMessageList[i];
	
				// Split the error message line by line
				String[] messageLines = errorMessageList[i].split("\\r?\\n");
	
				// Map to hold individual error elements
				Map<String, String> errorMap = new HashMap<>();
	
				// Hold error elements in key/value pairs
				for (String errorLine : messageLines) {
					errorMap.put(errorLine.substring(0, errorLine.indexOf(": ")).trim(),
							errorLine.substring(errorLine.indexOf(": ") + 2, errorLine.length()).trim());
				}
	
				// Add error map to the list
				listOfErrors.add(errorMap);
			}
		}
		return listOfErrors;
	}
}
