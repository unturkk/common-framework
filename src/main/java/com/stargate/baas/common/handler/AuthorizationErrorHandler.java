package com.stargate.baas.common.handler;

import org.springframework.stereotype.Component;

@Component
public class AuthorizationErrorHandler {
	/* 	This is where authorization specific errors will be handled.
		Example: 'Entity ID does not map to Gateway Company ID'
	*/
}
