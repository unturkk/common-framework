package com.stargate.baas.common.handler;

import java.util.List;
import java.util.Map;

import org.mule.api.MuleEventContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stargate.baas.common.constant.ErrorCodes;
import com.stargate.baas.common.constant.ErrorMessages;

@Component
public class JSONValidationHandler {

	@Autowired
	ErrorMessageHandler errorMessageHandler;

	public int setJSONValidationErrorsAndMessages(MuleEventContext eventContext) throws Exception {

		// All JSON Validation errors
		// return 400 status code
		int httpStatusCode = 400;

		// Error code list to be passed to DB
		List<String> errorCodeList = eventContext.getMessage().getInvocationProperty("varErrorCodeList");

		// Error message from incoming query parameter
		String errorMessage = eventContext.getMessage().getInvocationProperty("varErrorMessage");

		// List of maps for error key/value pairs
		List<Map<String, String>> listOfErrors = errorMessageHandler.getErrorMapList(errorMessage);

		// Add errors into varErrorMapList flow variable to be used
		// when placeholders are replaced
		eventContext.getMessage().setInvocationProperty("varErrorMapList", listOfErrors);

		// Go over the error list and find the right error code
		// by looking at the key phrases in the error explanations
		// and put error codes into each map so that they can be
		// identified later where placeholders are replaced
		for (int i = 0; i < listOfErrors.size(); i++) {
			listOfErrors.get(i).put("fieldName",
					listOfErrors.get(i).get("instance").substring(
							listOfErrors.get(i).get("instance").lastIndexOf("/") + 1,
							listOfErrors.get(i).get("instance").lastIndexOf("\"")));
			String error = listOfErrors.get(i).get("error");
			if (error.contains(ErrorMessages.FIELD_VALUE_TOO_LONG)) {
				errorCodeList.add(ErrorCodes.err_1400_06_004);
				listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_06_004);
				continue; // When error is identified skip the current iteration
			}
			if (error.contains(ErrorMessages.FIELD_VALUE_TOO_SHORT)) {
				if (listOfErrors.get(i).get("found").equals("0")) {
					errorCodeList.add(ErrorCodes.err_1400_05_001);
					listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_05_001);
				} else {
					errorCodeList.add(ErrorCodes.err_1400_06_001);
					listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_06_001);
				}
				continue;
			}
			if (error.contains(ErrorMessages.INCORRECT_DATA_TYPE)) {
				errorCodeList.add(ErrorCodes.err_1400_04_001);
				listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_04_001);
				continue;
			}
			if (error.contains(ErrorMessages.FIELD_VALUE_LOWER_THAN_REQUIRED_MINIMUM)) {
				// Separate error message for negative numbers
				if (Integer.parseInt(listOfErrors.get(i).get("minimum")) == 0) {
					errorCodeList.add(ErrorCodes.err_1400_07_002);
					listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_07_002);
				} else {
					errorCodeList.add(ErrorCodes.err_1400_07_003);
					listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_07_003);
				}
				continue;
			}
			if (error.contains(ErrorMessages.FIELD_VALUE_LOWER_THAN_REQUIRED_MAXIMUM)) {
				errorCodeList.add(ErrorCodes.err_1400_07_004);
				listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_07_004);
				continue;
			}
			if (error.contains(ErrorMessages.FIELD_VALUE_NOT_MULTIPLES_OF_GIVEN_NUMER)) {
				errorCodeList.add(ErrorCodes.err_1400_07_006);
				listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_07_006);
				continue;
			}
			if (error.contains(ErrorMessages.ARRAY_TOO_SHORT)) {
				errorCodeList.add(ErrorCodes.err_1400_08_001);
				listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_08_001);
				continue;
			}
			if (error.contains(ErrorMessages.ARRAY_TOO_LONG)) {
				errorCodeList.add(ErrorCodes.err_1400_08_002);
				listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_08_002);
				continue;
			}
			if (error.contains(ErrorMessages.ARRAY_ELEMENTS_MUST_BE_UNIQUE)) {
				errorCodeList.add(ErrorCodes.err_1400_08_003);
				listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_08_003);
				continue;
			}
			if (error.contains(ErrorMessages.NOT_FOUND_IN_ENUM)) {
				errorCodeList.add(ErrorCodes.err_1400_05_003);
				listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_05_003);
				continue;
			}
			if (error.contains(ErrorMessages.MISSING_FIELD)) {
				if (listOfErrors.get(i).get("instance").contains("request-header")) {
					errorCodeList.add(ErrorCodes.err_1400_03_004);
					listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_03_004);
					listOfErrors.get(i).put("fieldName",
							error.substring(error.indexOf("([\"") + 3, error.indexOf("\"])")).replace("\",\"", ", "));
				}
				if (listOfErrors.get(i).get("instance").contains("query-params")) {
					errorCodeList.add(ErrorCodes.err_1400_03_006);
					listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_03_006);
					listOfErrors.get(i).put("fieldName",
							error.substring(error.indexOf("([\"") + 3, error.indexOf("\"])")).replace("\",\"", ", "));
				}
				continue;
			}

			if (error.contains(ErrorMessages.UNEXPECTED_FIELD)) {
				if (listOfErrors.get(i).get("instance").contains("query-params")) {
					errorCodeList.add(ErrorCodes.err_1400_03_003);
					listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_03_003);
				}
				if (listOfErrors.get(i).get("instance").contains("request-header")) {
					errorCodeList.add(ErrorCodes.err_1400_03_001);
					listOfErrors.get(i).put("ErrorCode", ErrorCodes.err_1400_03_001);
				}
				listOfErrors.get(i).put("fieldName",
						listOfErrors.get(i).get("unwanted").replace("[\"", "").replace("\"]", ""));
				continue;
			}
		}
		return httpStatusCode;
	}
}
