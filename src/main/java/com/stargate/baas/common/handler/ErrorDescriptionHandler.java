package com.stargate.baas.common.handler;

import java.util.List;
import java.util.Map;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.springframework.beans.factory.annotation.Autowired;

import com.stargate.common.entity.ErrorCodeCrossRef;

public class ErrorDescriptionHandler implements Callable {
	
	@Autowired
	ErrorMessageHandler errorMessageHandler;
	
	public Object onCall(MuleEventContext eventContext) throws Exception {
		
		// Error Object List returned form DB
		ErrorCodeCrossRef[] errorCodeCrossRef = (ErrorCodeCrossRef[]) eventContext.getMessage().getPayload();
		
		/* Errors extracted from incoming query parameter
			
			Example map:
			
		    error: instance type (integer) does not match any allowed primitive type (allowed: ["string"])
	        level: "error"
	        schema: {"loadingURI":"..."}
	        instance: {"pointer":"/base_currency_code"}
	        domain: "validation"
	        keyword: "type"
	        found: "integer"
	        expected: ["string"]
	    */
		List<Map<String, String>> listOfErrors = eventContext.getMessage().getInvocationProperty("varErrorMapList");
		
		// Go over the list of errors and substitute placeholders 
		// with appropriate values
		for(Map<String, String> list : listOfErrors){
			
			// Replace value of instance with the field name
			String fieldName = list.get("fieldName");
			list.put("instance", fieldName);
			
			// TODO will be assigned to the correct field value
			// when response JSON gets changed to the desired format
			String fieldValue = null;
			
			String dataType = null;			
			
			int maxLength = 0;
			int minLength = 0;
			int minValue = 0;
			int maxValue = 0;
			int multipleOf = 0;
			int minNoOfArrayElements = 0;
			int maxNoOfArrayElements = 0;
			
			// Value of a field
			if (list.get("value") != null){
				fieldValue = list.get("value").replace("\"", "");
				list.put("value", fieldValue);
			} else {
				list.put("value", null);
			}
			// TODO test this !
			// Minimum length of a field
			if (list.get("minLength") != null){
				minLength = Integer.parseInt(list.get("minLength"));
			}
			// Maximum length of a field
			if (list.get("maxLength") != null){
				maxLength = Integer.parseInt(list.get("maxLength"));
			}
			// Minimum value for a field
			if (list.get("minimum") != null){
				minValue = Integer.parseInt(list.get("minimum"));
			}
			// Maximum value for a field
			if (list.get("maximum") != null){
				maxValue = Integer.parseInt(list.get("maximum"));
			}
			// Value for 'Multiples of'
			// Used when a number has to be multiples
			// of a given value
			if (list.get("divisor") != null){
				multipleOf = Integer.parseInt(list.get("divisor"));
			}
			// Minimum number of items in an array
			if (list.get("minItems") != null){
				minNoOfArrayElements = Integer.parseInt(list.get("minItems"));
			}
			// Maximum number of items in an array
			if (list.get("maxItems") != null){
				maxNoOfArrayElements = Integer.parseInt(list.get("maxItems"));
			}
			// Expected data type when wrong data type is passed
			// TODO this works correctly only when array has similar data types
			// such as Integer and number
			// satisfies our FX API but needs to be more generic
			if (list.get("expected") != null){
				if((list.get("expected").contains(",\""))){ // When allowed data types are more than one and passed as an array
					dataType = list.get("expected")
							.substring(list.get("expected").lastIndexOf(",\"")+2, 
									list.get("expected").lastIndexOf("\""));
				} else { // When only a single data type is allowed
					dataType = list.get("expected").replace("[\"", "").replace("\"]", "");
				}				
			}
			
			// Go over error descriptions and replace placeholders with correct values
			for(int i=0; i<errorCodeCrossRef.length; i++){
				if(list.get("ErrorCode").equals(errorCodeCrossRef[i].getChannelStatusCode())) {
					String description = errorCodeCrossRef[i].getDescription();
					String newDescription = null;
					newDescription = description.replace("[field_name]", "[" +fieldName +"]");
					newDescription = newDescription.replace("[array_name]", "[" +fieldName +"]");
					newDescription = newDescription.replace("[max]", "[" +maxLength +"]");
					newDescription = newDescription.replace("[min]", "[" +minLength +"]");
					newDescription = newDescription.replace("[min_value]", "[" +minValue +"]");
					newDescription = newDescription.replace("[max_value]", "[" +maxValue +"]");
					newDescription = newDescription.replace("[min_elements]", "[" +minNoOfArrayElements +"]");
					newDescription = newDescription.replace("[max_elements]", "[" +maxNoOfArrayElements +"]");
					newDescription = newDescription.replace("[multiple_of_value]", "[" +multipleOf +"]");
					newDescription = newDescription.replace("[data_type]", "[" +dataType +"]");
					//errorCodeCrossRef[i].setDescription(newDescription);
					list.put("description", newDescription);
					list.put("channelStatusCode", errorCodeCrossRef[i].getChannelStatusCode());
				}
			}
		}
		
		return eventContext.getMessage().getPayload();
	}
}
