package com.stargate.baas.common.constant;
/*
 * The class to hold String constants to be matched with
 * the error message that gets passed to the framework
 */
public class ErrorMessages {
	
	public static final String FIELD_VALUE_TOO_SHORT = "\" is too short";
	public static final String FIELD_VALUE_TOO_LONG = "\" is too long";
	public static final String INCORRECT_DATA_TYPE = "does not match any allowed primitive type";
	public static final String MISSING_FIELD = "has missing required properties";
	public static final String FIELD_VALUE_LOWER_THAN_REQUIRED_MINIMUM  = "is lower than the required minimum";
	public static final String FIELD_VALUE_LOWER_THAN_REQUIRED_MAXIMUM  = "is greater than the required maximum";
	public static final String FIELD_VALUE_NOT_MULTIPLES_OF_GIVEN_NUMER = "remainder of division is not zero";
	public static final String ARRAY_TOO_SHORT = "array is too short";
	public static final String ARRAY_TOO_LONG = "array is too long";
	public static final String ARRAY_ELEMENTS_MUST_BE_UNIQUE = "array must not contain duplicate elements";	
	public static final String NOT_FOUND_IN_ENUM = "not found in enum";
	public static final String UNEXPECTED_FIELD = "has properties which are not allowed";
	
}
