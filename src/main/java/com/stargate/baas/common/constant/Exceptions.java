package com.stargate.baas.common.constant;
/*
 * The class to hold String constants to be matched with
 * the Exceptions that gets passed to the framework
 */
public class Exceptions {
	
	public static final String METHOD_NOT_ALLOWED_EXCEPTION = 
			"org.mule.module.apikit.exception.MethodNotAllowedException";
	public static final String RESOURCE_NOT_FOUND_EXCEPTION = 
			"org.mule.module.apikit.exception.NotFoundException";
	
}