package com.stargate.baas.common.constant;
/*
 * The class to hold CHANNEL_STATUS_CODEs that are
 * use to query ERRORCODE_CROSS_REF table
 */
public final class ErrorCodes {

	public static final String err_1400_01_001 = "1400-01-001";
	public static final String err_1400_01_002 = "1400-01-002";

	public static final String err_1400_02_001 = "1400-02-001";
	public static final String err_1400_02_002 = "1400-02-002";
	public static final String err_1400_02_003 = "1400-02-003";
	public static final String err_1400_02_004 = "1400-02-004";
	public static final String err_1400_02_005 = "1400-02-005";

	public static final String err_1400_03_001 = "1400-03-001";
	public static final String err_1400_03_002 = "1400-03-002";
	public static final String err_1400_03_003 = "1400-03-003";
	public static final String err_1400_03_004 = "1400-03-004";
	public static final String err_1400_03_005 = "1400-03-005";
	public static final String err_1400_03_006 = "1400-03-006";
	public static final String err_1400_03_007 = "1400-03-007";
	public static final String err_1400_03_008 = "1400-03-008";

	public static final String err_1400_04_001 = "1400-04-001";
	public static final String err_1400_04_002 = "1400-04-002";
	public static final String err_1400_04_003 = "1400-04-003";
	public static final String err_1400_04_004 = "1400-04-004";
	
	public static final String err_1400_05_001 = "1400-05-001";
	public static final String err_1400_05_002 = "1400-05-002";
	public static final String err_1400_05_003 = "1400-05-003";

	public static final String err_1400_06_001 = "1400-06-001";
	public static final String err_1400_06_002 = "1400-06-002";
	public static final String err_1400_06_003 = "1400-06-003";
	public static final String err_1400_06_004 = "1400-06-004";

	public static final String err_1400_07_001 = "1400-07-001";
	public static final String err_1400_07_002 = "1400-07-002";
	public static final String err_1400_07_003 = "1400-07-003";
	public static final String err_1400_07_004 = "1400-07-004";
	public static final String err_1400_07_005 = "1400-07-005";
	public static final String err_1400_07_006 = "1400-07-006";

	public static final String err_1400_08_001 = "1400-08-001";
	public static final String err_1400_08_002 = "1400-08-002";
	public static final String err_1400_08_003 = "1400-08-003";

	public static final String err_1400_09_001 = "1400-09-001";
	public static final String err_1400_09_002 = "1400-09-002";
	public static final String err_1400_09_003 = "1400-09-003";

	public static final String err_1400_10_001 = "1400-10-001";

	public static final String err_1401_01_001 = "1401-01-001";
	public static final String err_1401_01_002 = "1401-01-002";
	public static final String err_1401_01_003 = "1401-01-003";

	public static final String err_1403_01_001 = "1403-01-001";
	public static final String err_1403_01_002 = "1403-01-002";
	public static final String err_1403_01_003 = "1403-01-003";
	public static final String err_1403_01_004 = "1403-01-004";
	public static final String err_1403_01_005 = "1403-01-005";
	public static final String err_1403_01_006 = "1403-01-006";
	public static final String err_1403_01_007 = "1403-01-007";

	public static final String err_1404_01_001 = "1404-01-001";
	public static final String err_1404_01_002 = "1404-01-002";
	public static final String err_1404_01_003 = "1404-01-003";
	
	// Doesn't exist in design but it should be nesessary
	public static final String err_1404_01_004 = "1404-01-004";

	public static final String err_1405_01_001 = "1405-01-001";

	public static final String err_1409_01_001 = "1409-01-001";

	public static final String err_1429_01_001 = "1429-01-001";
	public static final String err_1429_01_002 = "1429-01-002";

	public static final String err_1500_01_001 = "1500-01-001";
	public static final String err_1500_01_002 = "1500-01-002";
	public static final String err_1500_01_003 = "1500-01-003";
	public static final String err_1500_01_004 = "1500-01-004";
	public static final String err_1500_01_005 = "1500-01-005";
	public static final String err_1500_01_006 = "1500-01-006";
	public static final String err_1500_01_007 = "1500-01-007";
	public static final String err_1500_01_008 = "1500-01-008";
	public static final String err_1500_01_009 = "1500-01-009";
	public static final String err_1500_01_010 = "1500-01-010";
	public static final String err_1500_01_011 = "1500-01-011";

	public static final String err_1503_01_001 = "1503-01-001";
	public static final String err_1503_01_002 = "1503-01-002";
	public static final String err_1503_01_003 = "1503-01-003";
	public static final String err_1503_01_004 = "1503-01-004";

	public static final String err_1504_01_001 = "1504-01-001";
	public static final String err_1504_01_002 = "1504-01-002";
	public static final String err_1504_01_003 = "1504-01-003";
	public static final String err_1504_01_004 = "1504-01-004";

}
