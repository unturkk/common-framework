package com.stargate.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stargate.common.dao.ServicesDAO;
import com.stargate.common.entity.Services;

/*
 * This class is not being used in the Mule application
 */

@Repository("servicesDAO")
public class ServicesDAOImpl implements ServicesDAO {

	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private Properties queryProps;

	@Override
	public Services[] getAllServices() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Services getService() {

		String query = queryProps.getProperty("getService");

		Connection conn = null;
		Services services = null;

		try {
			conn = dataSource.getConnection();
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, "TEST_SRV_ID");
			ResultSet rs = stmt.executeQuery();

			services = new Services();

			if (rs.next()) {
				services.setServiceId(rs.getInt("SERVICE_ID"));
				services.setServiceResourceId(rs.getString("SERVICE_RESOURCE_ID"));
				services.setServiceName(rs.getString("SERVICE_NAME"));
				services.setDefaultMaskChar(rs.getString("DEFAULT_MASK_CHAR"));
				services.setDefaultMaskLength(rs.getInt("DEFAULT_MASK_LENGTH"));
			}
			stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return services;
	}
}
