package com.stargate.common.dao;

import com.stargate.common.entity.Services;

public interface ServicesDAO {
	
	public Services[] getAllServices();
	public Services getService();
	//public ?? getAllConfigs();
}
