package com.stargate.common.entity;

public class BackendErrorMapping {
	
	private int backEndConfigId;
	private int serviceId;
	private String backendFaultCode;
	private String description;
	
	public int getBackEndConfigId() {
		return backEndConfigId;
	}
	public void setBackEndConfigId(int backEndConfigId) {
		this.backEndConfigId = backEndConfigId;
	}
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	public String getBackendFaultCode() {
		return backendFaultCode;
	}
	public void setBackendFaultCode(String backendFaultCode) {
		this.backendFaultCode = backendFaultCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
