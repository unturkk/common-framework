package com.stargate.common.entity;

public class APIResource {
	
	private int apiResourceId;
	private String apiResourceName;
	private String apiResourceURI;
	private String apiResourceMethod;
	private String apiResourceEnvironment;
	private String apiResourceVersion;
	private String apiResourceCategory;
	private String lineOfBusinessId;
	private String apiResourceStatus;
	private String apiParentId;
	
	public int getApiResourceId() {
		return apiResourceId;
	}
	public void setApiResourceId(int apiResourceId) {
		this.apiResourceId = apiResourceId;
	}
	public String getApiResourceName() {
		return apiResourceName;
	}
	public void setApiResourceName(String apiResourceName) {
		this.apiResourceName = apiResourceName;
	}
	public String getApiResourceURI() {
		return apiResourceURI;
	}
	public void setApiResourceURI(String apiResourceURI) {
		this.apiResourceURI = apiResourceURI;
	}
	public String getApiResourceMethod() {
		return apiResourceMethod;
	}
	public void setApiResourceMethod(String apiResourceMethod) {
		this.apiResourceMethod = apiResourceMethod;
	}
	public String getApiResourceEnvironment() {
		return apiResourceEnvironment;
	}
	public void setApiResourceEnvironment(String apiResourceEnvironment) {
		this.apiResourceEnvironment = apiResourceEnvironment;
	}
	public String getApiResourceVersion() {
		return apiResourceVersion;
	}
	public void setApiResourceVersion(String apiResourceVersion) {
		this.apiResourceVersion = apiResourceVersion;
	}
	public String getApiResourceCategory() {
		return apiResourceCategory;
	}
	public void setApiResourceCategory(String apiResourceCategory) {
		this.apiResourceCategory = apiResourceCategory;
	}
	public String getLineOfBusinessId() {
		return lineOfBusinessId;
	}
	public void setLineOfBusinessId(String lineOfBusinessId) {
		this.lineOfBusinessId = lineOfBusinessId;
	}
	public String getApiResourceStatus() {
		return apiResourceStatus;
	}
	public void setApiResourceStatus(String apiResourceStatus) {
		this.apiResourceStatus = apiResourceStatus;
	}
	public String getApiParentId() {
		return apiParentId;
	}
	public void setApiParentId(String apiParentId) {
		this.apiParentId = apiParentId;
	}
}
