package com.stargate.common.entity;

import java.io.Serializable;

public class ErrorCodeCrossRef implements Serializable {

	private static final long serialVersionUID = 1162713842789614214L;
	
	private int exceptionConfigId;
	private String channelStatusCode;
	private String httpStatusCode;
	private String validationType;
	private String description;
	
	public int getExceptionConfigId() {
		return exceptionConfigId;
	}
	public void setExceptionConfigId(int exceptionConfigId) {
		this.exceptionConfigId = exceptionConfigId;
	}
	public String getChannelStatusCode() {
		return channelStatusCode;
	}
	public void setChannelStatusCode(String channelStatusCode) {
		this.channelStatusCode = channelStatusCode;
	}
	public String getHttpStatusCode() {
		return httpStatusCode;
	}
	public void setHttpStatusCode(String httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	public String getValidationType() {
		return validationType;
	}
	public void setValidationType(String validationType) {
		this.validationType = validationType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
