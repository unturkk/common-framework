package com.stargate.common.entity;

public class LogConfig {

	private int propertyLookupId;
	private int serviceId;
	private String propertyKey;
	private String propertyLookUpType;
	private String propertyValue;
	private String propertyDemiliter;
	
	public int getPropertyLookupId() {
		return propertyLookupId;
	}
	public void setPropertyLookupId(int propertyLookupId) {
		this.propertyLookupId = propertyLookupId;
	}
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	public String getPropertyKey() {
		return propertyKey;
	}
	public void setPropertyKey(String propertyKey) {
		this.propertyKey = propertyKey;
	}
	public String getPropertyLookUpType() {
		return propertyLookUpType;
	}
	public void setPropertyLookUpType(String propertyLookUpType) {
		this.propertyLookUpType = propertyLookUpType;
	}
	public String getPropertyValue() {
		return propertyValue;
	}
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}
	public String getPropertyDemiliter() {
		return propertyDemiliter;
	}
	public void setPropertyDemiliter(String propertyDemiliter) {
		this.propertyDemiliter = propertyDemiliter;
	}
}
