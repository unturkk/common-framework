package com.stargate.common.entity;

public class Services {

	private int serviceId;
	private String serviceResourceId;
	private String serviceName;
	private String defaultMaskChar;
	private int defaultMaskLength;
	
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceResourceId() {
		return serviceResourceId;
	}
	public void setServiceResourceId(String serviceResourceId) {
		this.serviceResourceId = serviceResourceId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getDefaultMaskChar() {
		return defaultMaskChar;
	}
	public void setDefaultMaskChar(String defaultMaskChar) {
		this.defaultMaskChar = defaultMaskChar;
	}
	public int getDefaultMaskLength() {
		return defaultMaskLength;
	}
	public void setDefaultMaskLength(int defaultMaskLength) {
		this.defaultMaskLength = defaultMaskLength;
	}	
}
