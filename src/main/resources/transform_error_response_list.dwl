%dw 1.0
%output application/json
---
errors: (
	payload map  {
		error_code: flowVars.varErrorMapList[$$].channelStatusCode,
		description: flowVars.varErrorMapList[$$].description,
		(field_name: flowVars.varErrorMapList[$$].instance) 
			when flowVars.varErrorMapList[$$].instance?,
		(field_value: flowVars.varErrorMapList[$$].value)
			when flowVars.varErrorMapList[$$].value?,
		api_specification_url: "URL?"		
	}
)