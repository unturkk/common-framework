%dw 1.0
%output application/json
---
payload map ((payload01 , indexOfPayload01) -> {
	channelStatusCode: payload01.channel_status_code,
	description: payload01.description,
	exceptionConfigId: payload01.exception_config_id as :number,
	httpStatusCode: payload01.http_status_code,
	validationType: payload01.validation_type
})