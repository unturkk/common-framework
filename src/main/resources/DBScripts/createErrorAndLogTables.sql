#create schema gateway_mediation;

create table BACKEND_ERROR_MAPPING (
	BACKEND_CONFIG_ID 	int(11) auto_increment,
    SERVICE_ID 			int(11),
    EXCEPTION_CONFIG_ID int(11),
    BACKEND_FAULT_CODE 	varchar(20),
    DESCRIPTION 		varchar(20),
    primary key(BACKEND_CONFIG_ID)
);

create table ERRORCODE_CROSS_REF (
	EXCEPTION_CONFIG_ID int(11) auto_increment,
    CHANNEL_STATUS_CODE varchar(20),
    HTTP_STATUS_CODE 	varchar(20),
    VALIDATION_TYPE 	varchar(200),
    DESCRIPTION 		varchar(1000),
    primary key(EXCEPTION_CONFIG_ID)
);

create table LOG_CONFIG (
	PROPERTY_LOOKUP_ID 		 int(11) auto_increment,
    SERVICE_ID 				 int(11),
    PROPERTY_KEY 			 varchar(50),
    PROPERTY_LOOKUP_TYPE 	 varchar(100),
    PROPERTY_VALUE 			 varchar(250),
    PROPERTY_VALUE_DELIMITER varchar(20),
    primary key(PROPERTY_LOOKUP_ID)
);

create table SERVICES (
	SERVICE_ID 			int(11) auto_increment,
    SERVICE_RESOURCE_ID varchar(50),
    SERVICE_NAME 		varchar(50),
    DEFAULT_MASK_CHAR 	char(10),
    DEFAULT_MASK_LENGTH int(11),
    primary key(SERVICE_ID)
);

create table API_RESOURCE (
	API_RESOURCE_ID 		 int(11) auto_increment,
    API_RESOURCE_NAME 		 varchar(100),
    API_RESOURCE_URI 		 varchar(100),
    API_RESOURCE_METHOD 	 varchar(100),
    API_RESOURCE_ENVIRONMENT varchar(100),
    API_RESOURCE_VERSION 	 varchar(100),
    API_RESOURCE_CATEGORY 	 varchar(100),
    LINE_OF_BUSINESS_ID 	 varchar(100),
    API_RESOURCE_STATUS 	 varchar(100),
    API_PARENT_ID 			 varchar(100),
    primary key(API_RESOURCE_ID)
);
