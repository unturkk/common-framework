INSERT INTO ERRORCODE_CROSS_REF (CHANNEL_STATUS_CODE, HTTP_STATUS_CODE, VALIDATION_TYPE, DESCRIPTION)
VALUES
######################################## PART I ################################################################
('1400-01-001', '400', 'JSON structure is invalid - DataPower', 'JSON structure is invalid.'), 
('1400-01-002', '400', 'JSON structure is invalid - WSO2', 'JSON structure is invalid.'),
################################################################################################################ 
('1400-02-001', '400', 'Request is too big', 'There is a problem with the request.'), 
('1400-02-002', '400', 'Threat detected - DataPower', 'There is a problem with the request.'), 
('1400-02-003', '400', 'Threat detected - WSO2', 'There is a problem with the request.'), 
('1400-02-004', '400', 'Threat detected - XML object', 'There is a problem with the request.'), 
('1400-02-005', '400', 'Threat detected - embedded object (non-XML)', 'There is a problem with the request.'),
################################################################################################################ 
('1400-03-001', '400', 'Unknown field in header', '[field_name] is an invalid field in the request header.'), #DONE/DEBUG - NOT SURE
('1400-03-002', '400', 'Unknown field in body', '[field_name] is an invalid field in the request body.'), #DONE/DEBUG - NOT SURE
('1400-03-003', '400', 'Unknown parameter in query string', '[field_name] is an invalid parameter in the query string.'), #DONE
('1400-03-004', '400', 'Missing required field in header', '[field_name] is missing from the request header.'), #DONE
('1400-03-005', '400', 'Missing required field in body', '[field_name] is missing from the request body.'), #TODO - NOT SURE
('1400-03-006', '400', 'Missing required parameter in query string', '[field_name] is missing from the query string.'), #DONE
('1400-03-007', '400', 'Missing body from request', 'JSON body is invalid.'),
('1400-03-008', '400', 'Missing conditionally-required field/parameter', '[conditionally required field_name] must be included if [field_name] is sent in the request.'),
################################################################################################################
('1400-04-001', '400', 'Incorrect data type', 'The value for [field_name] is not a valid [data_type].'), #DONE
('1400-04-002', '400', 'Value does not match the pattern', 'The value for [field_name] does not match the pattern: [regex].'),
('1400-04-003', '400', 'Invalid URI', 'The value for [field_name] is an invalid URI per RFC 3986.'),
('1400-04-004', '400', 'Invalid email address', 'The value for [field_name] is an invalid email address per RFC 5322.'),
################################################################################################################
('1400-05-001', '400', 'Blank value not allowed', 'The value for [field_name] cannot be blank.'), #DONE
('1400-05-002', '400', 'Null value not allowed', 'The value for [field_name] cannot be null.'),
('1400-05-003', '400', 'Value is not in the list of allowed values', 'The value for [field_name] is not in the list of allowed values.'), #DONE
########################################### PART II ############################################################
('1400-06-001', '400', 'Incorrect length - too few characters', 'The value for [field_name] has to be at least [min] characters.'),  #DONE -TWEAKED FROM The value for [field_name] must be between [min] and [max] characters.
('1400-06-002', '400', 'Incorrect length - value must be an exact length', 'The value for [field_name] must be [exact_length] characters.'), #TODO - NOT SURE
('1400-06-003', '400', 'Incorrect length - two exact length options', 'The value for [field_name] must be either [length1] or [length2] characters.'), #TODO - NOT SURE
('1400-06-004', '400', 'Incorrect length - too many characters', 'The value for [field_name] cannot be more than [max] characters.'),  #DONE -TWEAKED FROM The value for [field_name] must be between [min] and [max] characters.
################################################################################################################
('1400-07-001', '400', 'Value outside of range of allowed values', 'The value for [field_name] must be between [min] and [max].'), #TODO - NOT SURE
('1400-07-002', '400', 'Number field cannot be a negative number', 'The value for [field_name] cannot be a negative number.'), #DONE
('1400-07-003', '400', 'Number field must be greater than a minimum number', 'The value for [field_name] must be greater than [min_value].'), #DONE
('1400-07-004', '400', 'Number field must be less than a maximum number', 'The value for [field_name] must be less than [max_value].'), #DONE
('1400-07-005', '400', 'Max value is less than min value for a range', 'The value for [max value field_name] cannot be less than the value for [min value field_name].'), #TODO - NOT SURE
('1400-07-006', '400', 'Value must be a multiple of [multiple_of_value]', 'The value for [field_name] must be a multiple of [multiple_of_value].'), #DONE
################################################################################################################
('1400-08-001', '400', 'Array must have at least minimum number of elements', '[array_name] must contain at least [min_elements] element(s).'), #DONE - TWEAKED THIS ONE
('1400-08-002', '400', 'Array cannot have more than [max] elements', '[array_name] cannot contain more than [max_elements] elements.'), #DONE
('1400-08-003', '400', 'Each array element must be unique', 'Each elemen in the [array_name] array must be unique.'), #DONE
################################################################################################################
('1400-09-001', '400', 'Date cannot be in the future', 'The value for [date field_name] cannot be a date in the future.'), #TODO - NOT SURE
('1400-09-002', '400', 'Date cannot be in the past', 'The value for [date field_name] cannot be a date in the past.'), #TODO - NOT SURE
('1400-09-003', '400', 'End date cannot be before start date', 'The value for [end date field_name] cannot be a earlier than the value for [start date field_name].'), #TODO - NOT SURE
################################################################################################################
('1400-10-001', '400', 'XML schema validation error', 'The XML file does not match the schema.'),
################################################################################################################
('1401-01-001', '401', 'Access token is invalid or expired', 'The token to access the requested resource is invalid or expired.'),
('1401-01-002', '401', 'Access token is missing', 'The access token is not included in the request.'),
('1401-01-003', '401', 'Missing or invalid client certificate - DataPower', 'Missing or invalid client certificate.'),
################################################################################################################
('1403-01-001', '403', 'Token does not have access to API', 'The token is not authorized to access the requested API.'),
('1403-01-002', '403', 'Token does not have access to resource', 'The token is not authorized to access the requested resource.'),
('1403-01-003', '403', 'Customer not authorized for the service in backend system', 'The account associated with the token is not authorized to access the requested service.'),
('1403-01-004', '403', 'Token does not map to Gateway Company ID', 'Invalid Gateway Company ID.'),
('1403-01-005', '403', 'Token to access backend service is invalid or expired', 'The token to access the requested service is invalid or expired.'),
('1403-01-006', '403', 'Entity ID does not map to Gateway Company ID', 'Invalid Entity ID.'),
('1403-01-007', '403', 'Service agreement expired', 'Your service agreement has expired.Contact Wells Fargo Gateway support 844-WFG8WAY or gateway@wellsfargo.com to obtain an updated service agreement.'),
##################################### PART III ##################################################################
('1404-01-001', '404', 'API does not exist - DataPower', 'The requested API does not exist.'),
('1404-01-002', '404', 'API does not exist - WSO2', 'The requested API does not exist.'),
('1404-01-003', '404', 'Resource was deprecated', 'The API version requested is no longer active. Check the API specification for an updated version.'),
('1404-01-004', '404', 'Resource not found - added by unturk', 'Resource not found.'), #DONE
################################################################################################################
('1405-01-001', '405', 'Method not supported', 'The requested HTTP method is not supported. Check the API specification for supported methods.'), #DONE
################################################################################################################
('1409-01-001', '409', 'Request conflicts with the current state of the resource', 'Current state of the resource does not permit the request to be processed. Please try again.'),
################################################################################################################
('1429-01-001', '429', 'Too many requests within a certain time frame - DataPower', 'The request cannot be procesed because of rate limiting.'),
('1429-01-002', '429', 'Too many requests within a certain time frame - WSO2', 'The request cannot be procesed because of rate limiting.'),
################################################################################################################
('1500-01-001', '500', 'Internal server error - DataPower', 'Internal server error.'),
('1500-01-002', '500', 'Internal server error - WSO2', 'Internal server error.'),
('1500-01-003', '500', 'Internal server error - Mulesoft', 'Internal server error.'),
('1500-01-004', '500', 'Internal server error - backend system', 'Internal server error.'), #DONE
('1500-01-005', '500', 'Connection problem between Gateway and backend system', 'Internal server error.'),
('1500-01-006', '500', 'Problem with request send to backend system', 'Internal server error.'),
('1500-01-007', '500', 'Blank body returned from backend system', 'Internal server error.'),
('1500-01-008', '500', 'Unknown error - DataPower', 'Internal server error.'),
('1500-01-009', '500', 'Unknown error - WSO2', 'Internal server error.'),
('1500-01-010', '500', 'Unknown error - MuleSoft', 'Internal server error.'),
('1500-01-011', '500', 'Unknown error - backend system', 'Internal server error.'),
################################################################################################################
('1503-01-001', '503', 'Service unavailable - DataPower', 'Service unavailable.'),
('1503-01-002', '503', 'Service unavailable - WSO2', 'Service unavailable.'),
('1503-01-003', '503', 'Service unavailable - MuleSoft', 'Service unavailable.'),
('1503-01-004', '503', 'Service unavailable - backend system', 'Service unavailable.'),
################################################################################################################
('1504-01-001', '504', 'Server timeout - DataPower', 'Server timeout.'),
('1504-01-002', '504', 'Server timeout - WSO2', 'Server timeout.'),
('1504-01-003', '504', 'Server timeout - MuleSoft', 'Server timeout.'),
('1504-01-004', '504', 'Server timeout - backend system', 'Server timeout.');